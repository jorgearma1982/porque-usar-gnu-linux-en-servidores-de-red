#
# Makefile para construir documentos HTML
#

NOMBRE_ARTICULO="porque-usar-gnu-linux-en-servidores-de-red"

.PHONY: test build clean help

all: help

# Validar el archivo indice, este a su vez validará los demas capitulos
test:
	xmllint --dtdvalid http://www.oasis-open.org/docbook/xml/4.2/docbookx.dtd --noout --valid index.xml

# Convertir el documento a HTML y almacenarlo en el directorio "html"
build:
	xsltproc --output html/index.html \
                --stringparam toc.section.depth 4 \
		--stringparam chunker.output.encoding UTF-8 \
		--stringparam chunker.output.indent yes \
                --stringparam html.stylesheet html.css \
                --stringparam admon.graphics 1 \
                --stringparam navig.graphics 1 \
		/usr/share/xml/docbook/stylesheet/nwalsh/xhtml/onechunk.xsl \
                index.xml
	cp -v css/html.css html/
	cp -v GnuCopyright.htm html/
	cp -r imagenes html/
	cp -r /usr/share/xml/docbook/stylesheet/nwalsh/images html/

# Limpiando archivos de respaldos y html.
clean:
	echo "Limpiando archivos de respaldos y html"
	find . -name \*~ -exec rm \{\} \;
	rm -rfv html/*

help:
	@echo ""
	@echo "Please use \`make <target>' where <target> is one of"
	@echo ""
	@echo "  test                   Validates the XML file."
	@echo "  build			Build HTML artifact."
	@echo "  clean                  Cleans local changes and temporary files."
	@echo ""
	@echo ""
