<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN"
"http://www.oasis-open.org/docbook/xml/4.2/docbookx.dtd">
<article lang="es">
  <articleinfo>
    <title>Porque usar GNU/Linux en servidores de red</title>

    <authorgroup>
      <author>
        <firstname>Jorge Armando</firstname>

        <surname>Medina</surname>

        <affiliation>
          <orgname>Sistemas LinuxRed.</orgname>

          <orgdiv>Documentación Técnica</orgdiv>
        </affiliation>
      </author>
    </authorgroup>

    <revhistory>
      <revision>
        <revnumber>0.1</revnumber>

        <date>27 Sep 2014</date>

        <authorinitials>jm - jmedina@sistemaslinuxred.com.mx</authorinitials>

        <revremark>Versión inicial.</revremark>
      </revision>

      <revision>
        <revnumber>0.50</revnumber>

        <date>28 Sep 2014</date>

        <authorinitials>jm - jmedina@sistemaslinuxred.com.mx</authorinitials>

        <revremark>Correcciones menores..</revremark>
      </revision>
    </revhistory>

    <pubdate><?dbtimestamp format="Y/m/d"?></pubdate>

    <copyright>
      <year>2014</year>

      <holder>Jorge Armando Medina</holder>
    </copyright>

    <legalnotice>
      <para>Se otorga permiso para copiar, distribuir y/o modificar éste
      documento bajo los términos de la Licencia de Documentación Libre GNU,
      Versión 1.2 o cualquier otra posterior publicada por la Fundación de
      Software Libre; sin secciones invariantes, sin textos en portada y
      contraportada. Una copia de la licencia se incluye en la sección
      titulada <quote><ulink url="GnuCopyright.htm">Licencia de Documentación
      Libre GNU</ulink></quote>.</para>
    </legalnotice>

    <abstract>
      <para>En este articulo explicare porque recomendamos usar GNU/Linux como
      sistema operativo para servidores de red, ya sea para uso personal o
      empresarial. Empezaremos hablando de analizar los requisitos para
      implementar un servidor de red, porque usar GNU/Linux, y al final porque
      usar la distribución Ubuntu Server para servidores de red.</para>
    </abstract>
  </articleinfo>

  <section>
    <title>Introducción</title>

    <para>En este articulo explicare porque recomendamos usar GNU/Linux como
    sistema operativo para servidores de red, ya sea para uso personal o
    empresarial. Empezaremos hablando de analizar los requisitos para
    implementar un servidor de red, porque usar GNU/Linux, y al final porque
    usar la distribución Ubuntu Server para servidores de red.</para>
  </section>

  <section>
    <title>Objetivos</title>

    <para>Mostrar las ventajas tanto a nivel negocio como a nivel técnico de
    utilizar sistemas GNU/Linux en servidores de red en entornos de uso
    personal y empresariales.</para>
  </section>

  <section>
    <title>Audiencia</title>

    <para>Este documento esta enfocado a ayudar a administradores de sistemas,
    desarrolladores, o gerentes de areas de TI en la toma de desiciones para
    elejir inteligentemente que sistema operativo utilizar en la
    infraestructura de TI de las empresas.</para>
  </section>

  <section>
    <title>Analizando los requisitos para la implementación de un servidor de
    red</title>

    <para>Antes de ponerse a instalar un sistema operativo en un servidor es
    recomendable hacer una pausa y pensar en los requisitos del cliente, donde
    el cliente podemos ser nosotros mismos, nuestros jefes en la oficina o
    algún cliente de otra empresa. Es decir, debemos realizar un análisis de
    requisitos.</para>

    <para>Es importante dejar bien en claro y por escrito (el inicio de la
    documentación) cuales son los requisitos, tanto los funcionales y los no
    funcionales, se debe establecer cuales son los problemas que se quieren
    resolver, de ser necesario cuales son los requisitos del negocio y también
    los requisitos técnicos.</para>

    <para>Algunos de los requisitos de negocio más comunes pueden ser:</para>

    <itemizedlist>
      <listitem>
        <para>Reducir los costos de operación en relación a la infraestructura
        de TI.</para>
      </listitem>
    </itemizedlist>

    <itemizedlist>
      <listitem>
        <para>Interconectar los equipos de computo usando una infraestructura
        de TI abierta y estándar la cual permita hacer un uso más eficiente de
        los recursos.</para>
      </listitem>
    </itemizedlist>

    <itemizedlist>
      <listitem>
        <para>Centralizar el almacenamiento de los documentos de oficina y
        datos de aplicaciones de la empresa.</para>
      </listitem>
    </itemizedlist>

    <itemizedlist>
      <listitem>
        <para>Controlar el acceso de usuarios y grupos a los recursos
        compartidos en la red de forma eficiente y segura.</para>
      </listitem>
    </itemizedlist>

    <itemizedlist>
      <listitem>
        <para>Mejorar la colaboración entre usuarios usando el servicio de
        compartición de archivos e impresoras en red.</para>
      </listitem>
    </itemizedlist>

    <itemizedlist>
      <listitem>
        <para>Mejorar las comunicaciones entre los colaboradores usando el
        servicio de correo electrónico.</para>
      </listitem>
    </itemizedlist>

    <para>Lo anterior se puede traducir en los siguientes requisitos
    técnicos:</para>

    <itemizedlist>
      <listitem>
        <para>Crear una red LAN para conectar los equipos de computo y otros
        dispositivos de red, de forma que los usuarios puedan mejorar la
        productividad colaborando de forma eficiente con el uso de las
        tecnologías de la información y comunicaciones.</para>
      </listitem>
    </itemizedlist>

    <itemizedlist>
      <listitem>
        <para>Implementar los servicios de infraestructura de red básicos para
        el control lógico de la red.</para>
      </listitem>
    </itemizedlist>

    <itemizedlist>
      <listitem>
        <para>Implementar un sistema controlador de dominio para centralizar
        la administración de usuarios y grupos, así como el acceso a los
        recursos de la red.</para>
      </listitem>
    </itemizedlist>

    <itemizedlist>
      <listitem>
        <para>Implementar un servidor para ofrecer servicios de compartición
        de archivos e impresoras en red.</para>
      </listitem>
    </itemizedlist>

    <itemizedlist>
      <listitem>
        <para>Implementar un servidor de red para ofrecer servicios de correo
        electrónico. </para>
      </listitem>
    </itemizedlist>

    <para>Después de tener bien claros los requisitos, solo entonces podemos
    empezar a pensar en el diseño de la red, y planificar la instalación de
    servidores y servicios de red.</para>
  </section>

  <section>
    <title>Porque usar el sistema operativo GNU/Linux para un servidor de
    red</title>

    <para>Los sistemas operativos GNU/Linux están basados en los principios
    del software libre, dicho software respeta la libertad de los usuarios y
    la comunidad, ofreciendo las siguientes libertades:</para>

    <itemizedlist>
      <listitem>
        <para>La libertad de <emphasis role="bold">ejecutar</emphasis> el
        programa como se desea, con cualquier propósito (libertad 0).</para>
      </listitem>
    </itemizedlist>

    <itemizedlist>
      <listitem>
        <para>La libertad de <emphasis role="bold">estudiar</emphasis> cómo
        funciona el programa, y cambiarlo para que haga lo que usted quiera
        (libertad 1). El acceso al código fuente es una condición necesaria
        para ello.</para>
      </listitem>
    </itemizedlist>

    <itemizedlist>
      <listitem>
        <para>La libertad de <emphasis role="bold">redistribuir</emphasis>
        copias para ayudar a su prójimo (libertad 2).</para>
      </listitem>
    </itemizedlist>

    <itemizedlist>
      <listitem>
        <para>La libertad de distribuir copias de sus versiones <emphasis
        role="bold">modificadas</emphasis> a terceros (libertad 3). Esto le
        permite ofrecer a toda la comunidad la oportunidad de beneficiarse de
        las modificaciones. El acceso al código fuente es una condición
        necesaria para ello.</para>
      </listitem>
    </itemizedlist>

    <para>En grandes líneas, significa que los usuarios tienen la libertad
    para ejecutar, copiar, distribuir, estudiar, modificar y mejorar el
    software. Es decir, el «software libre» es una cuestión de libertad, no de
    precio. Para entender el concepto, piense en «libre» como en «libre
    expresión», no como en «barra libre».</para>

    <para>Además de esto, el kernel Linux es el proyecto de software que ha
    unido a más gente en la historia de la humanidad como ningún otro
    proyecto. Esto significa que alrededor del mundo hay miles de
    programadores, usuarios, educadores, comerciantes y toda clase de personas
    dedicadas a fomentar el uso de las tecnologías abiertas y los principios
    del software libre, todo para el beneficio de la humanidad.</para>

    <para>Al tener a tanta gente trabajando en el mismo objetivo obtenemos un
    sistema operativo que ofrece las siguientes funcionalidades:</para>

    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">Escalabilidad:</emphasis> Los sistemas
        GNU/Linux permiten escalar para ofrecer servicios para usuarios en
        casa, empresas de todos tamaños, y hasta para trabajos
        científicos.</para>
      </listitem>
    </itemizedlist>

    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">Disponibilidad:</emphasis> Los sistemas
        GNU/Linux ofrecen tasas de disponibilidad para soportar todo tipo de
        demanda.</para>
      </listitem>
    </itemizedlist>

    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">Rendimiento:</emphasis> Los sistemas
        GNU/Linux ofrecen un buen rendimiento de forma predeterminada, pero
        además permiten optimizarlos para todo tipo de demandas.</para>
      </listitem>
    </itemizedlist>

    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">Seguridad:</emphasis> Los sistemas
        GNU/Linux son seguros de forma predeterminada, y cuando se presenta un
        problema de seguridad, la comunidad los arregla rápidamente teniendo
        las correcciones en poco tiempo.</para>
      </listitem>
    </itemizedlist>

    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">Administrabilidad:</emphasis> Los sistemas
        GNU/Linux permite ser administrados usando diferentes métodos, tanto
        en línea de comandos como con interfaces gráficas, o incluso usando
        herramientas dedicadas a configuraciones centralizadas y
        automatizadas.</para>
      </listitem>
    </itemizedlist>

    <itemizedlist>
      <listitem>
        <para>F<emphasis role="bold">acilidad de Uso:</emphasis> A pesar de la
        mala fama, los sistemas GNU/Linux son fáciles de usar, siempre y
        cuando se tenga la formación adecuada.</para>
      </listitem>
    </itemizedlist>

    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">Adaptabilidad:</emphasis> Los sistemas
        GNU/Linux pueden adaptarse a los cambios continuos que se presentan
        cada día.</para>
      </listitem>
    </itemizedlist>

    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">Asequible:</emphasis> Los sistemas
        GNU/Linux, al ser software libre no tienen costos de licencias.</para>
      </listitem>
    </itemizedlist>

    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">Balance:</emphasis> Los sistemas GNU/Linux
        ofrecen el mejor balance costo beneficio.</para>
      </listitem>
    </itemizedlist>

    <para/>
  </section>

  <section>
    <title>Porque elegir la distribución GNU/Linux Ubuntu Server para un
    servidor de red</title>

    <para>Algunas de las razones por las que Ubuntu Server Edition es
    recomendado para servidores de red:</para>

    <itemizedlist>
      <listitem>
        <para>Ubuntu Server Edition es una distribución GNU/Linux orientada a
        servidores, esta basado en Debian y soporta los mismos paquetes y
        configuraciones.</para>
      </listitem>
    </itemizedlist>

    <itemizedlist>
      <listitem>
        <para>Ubuntu Server Edition esta respaldado y soportado por la empresa
        Canonical.</para>
      </listitem>
    </itemizedlist>

    <itemizedlist>
      <listitem>
        <para>El equipo de Ubuntu Server mantiene ciclos de control de calidad
        adicionales a los de Debian.</para>
      </listitem>
    </itemizedlist>

    <itemizedlist>
      <listitem>
        <para>Ubuntu Server LTS tiene un ciclo de vida de 5 años con soporte
        de actualizaciones de seguridad.</para>
      </listitem>
    </itemizedlist>

    <itemizedlist>
      <listitem>
        <para>Ubuntu server esta certificado para servidores DELL PowerEdge y
        HP Proliant entre otras marcas.</para>
      </listitem>
    </itemizedlist>

    <itemizedlist>
      <listitem>
        <para>El equipo de Ubuntu Security se encarga de desarrollar e
        integrar herramientas de seguridad a nivel kernel y aplicación, darle
        seguimiento a las vulnerabilidades de seguridad que afectan los
        paquetes soportados realizando auditorías, rastreos, corrección y
        pruebas.</para>
      </listitem>
    </itemizedlist>

    <itemizedlist>
      <listitem>
        <para>Ubuntu Server incluye soporte completo de virtualización basada
        en KVM el cual soporta la mayoría de funciones incluidas en
        Xen.</para>
      </listitem>
    </itemizedlist>

    <itemizedlist>
      <listitem>
        <para>Ubuntu Server LTS tiene herramientas para integrar plataformas
        Cloud privadas basadas en KVM.</para>
      </listitem>
    </itemizedlist>

    <para>Para conocer más acerca de Ubuntu server les recomiendo leer los
    brochures para las últimas versiones LTS.</para>
  </section>

  <section>
    <title>Recursos adicionales</title>

    <para>Para obtener más información, consulte los siguientes recursos que
    puede encontrar tanto en el sistema local o en línea:</para>

    <itemizedlist>
      <listitem>
        <para>Recomendamos leer la documentación en línea para conocer más
        acerca de Ubuntu Server:</para>

        <itemizedlist>
          <listitem>
            <para>Requisitos no funcionales: <ulink
            url="http://es.wikipedia.org/wiki/Requisito_no_funcional">http://es.wikipedia.org/wiki/Requisito_no_funcional</ulink></para>
          </listitem>

          <listitem>
            <para>Requisitos funcionales: <ulink
            url="http://es.wikipedia.org/wiki/Requisito_funcional">http://es.wikipedia.org/wiki/Requisito_funcional</ulink></para>
          </listitem>

          <listitem>
            <para>Que es el software libre?: <ulink
            url="http://www.gnu.org/philosophy/free-sw.es.html">http://www.gnu.org/philosophy/free-sw.es.html</ulink></para>
          </listitem>

          <listitem>
            <para>Kernel Linux: <ulink
            url="https://www.kernel.org/">https://www.kernel.org/</ulink></para>
          </listitem>

          <listitem>
            <para>Why Linux is Better?: <ulink
            url="http://www.whylinuxisbetter.net">http://www.whylinuxisbetter.net</ulink></para>
          </listitem>

          <listitem>
            <para>Ubuntu Server: <ulink
            url="http://www.ubuntu.com/server">http://www.ubuntu.com/server</ulink></para>
          </listitem>
        </itemizedlist>
      </listitem>
    </itemizedlist>

    <para/>
  </section>
</article>
